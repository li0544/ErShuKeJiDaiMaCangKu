# http工具集

## v1.2.6
- 增加打开.har文件的功能(firefox|chrome控制台导出数据)(将文件拖拽至窗口可直接打开)；
- 增加图片和base64互转的功能(将图片拖拽至窗口可直接打开并转换)；

![v1.2.6](demo_v1.2.6_1.png)
![v1.2.6](demo_v1.2.6_2.png)

## v1.2.2
- 增加在线执行多种编程语言的功能；
- 增加AI功能；

![v1.2.2](demo_1.2.2.png)

## v1.1

![v1.1](demo.png)

# 打包说明

### 0x01 安装sip 
```
pip install sip
```
### 0x02 安装pyqt5 
```
pip install pyqt5
```
### 0x03 安装pyinstaller 
```
pip install pyinstaller
```

### 0x04 运行httpTool_build.bat可打包生成exe
    
### 0x05 运行httpTool_run.bat可通过pythonw执行该脚本
    
### 0x06 如果打包出错请检查sip，如果sip已安装，可以卸载重装

### 0x07 重新打包请删除build临时目录