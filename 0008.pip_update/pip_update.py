'''
water
2017-09-14
'''

import os

if __name__ == "__main__":
    s = os.popen("pip list --outdated").read()

    f = open("log.txt", "w")
    f.write(s)
    f.close()
    #print(s)

    ls = s.split("\n")
    for l in ls:
        print(l)
        r = l.split()
        if len(r) == 6:
            #print(r)
            s_n = r[0]
            s_cmd = "pip install --upgrade %s" % s_n
            print("\t%s" % s_cmd)
            os.system(s_cmd)
