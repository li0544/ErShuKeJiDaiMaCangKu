# 获取arduino所有版本下载地址
![](p1.png)

# 增加windows和linux中打包生成可执行文件的脚本
> 需要安装pyinstaller

```
pip3 install pyinstaller
```
> 生成可执行文件
- windows中
```
wpdlarduino_build.bat
```
- linux中
```
./wpdlarduino_build.sh
```
> 运行
- windows中
```
wpdlarduino
```
- linux中
```
./wpdlarduino
```
> 运行结果
```
get arduino nodes
water
2018-01-11
version 1.1.1
wpdlarduino [-winzip] [-winexe] [-mac_os] [-linux_32] [-linux_64] [-linux_arm]
    -winzip         arduino-xxxx-windows.zip
    -winexe         arduino-xxxx-windows.exe
    -mac_os         arduino-xxxx-macosx.zip
    -linux_32       arduino-xxxx-linux32.tar.xz
    -linux_64       arduino-xxxx-linux64.tar.xz
    -linux_arm      arduino-xxxx-linuxarm.tar.xz
    -v/--version    version
    -h/--help       help
```
> 首次运行会生成config_arduino.ini配置文件，里面记录了缓存目录，配置文件可根据需要手动编辑

# wpdlarduino使用举例

> 获取windows可用的IDE列表
```
wpdlarduino -winzip
```
> 获取linux64位系统可用的IDE列表
```
wpdlarduino -linux_64
```
> 获取树莓派可用的IDE列表
```
wpdlarduino -linux_arm
```
> 下载举例
```
wget -c  http://downloads.arduino.cc/arduino-x.x-xxxxx.xxx
```
