import os
import sys
import requests
import configparser

readme = '''
get arduino nodes
water
2018-01-11
version %s
wpdlarduino [-winzip] [-winexe] [-mac_os] [-linux_32] [-linux_64] [-linux_arm]
    -winzip         arduino-xxxx-windows.zip
    -winexe         arduino-xxxx-windows.exe
    -mac_os         arduino-xxxx-macosx.zip
    -linux_32       arduino-xxxx-linux32.tar.xz
    -linux_64       arduino-xxxx-linux64.tar.xz
    -linux_arm      arduino-xxxx-linuxarm.tar.xz
    -v/--version    version
    -h/--help       help
'''
s_version = "1.1.1"

root      = "arduino"
home      = "http://downloads.arduino.cc/"
fn_config = "config_arduino.ini"

winzip    = "arduino-%s-windows.zip"
winexe    = "arduino-%s-windows.exe"
mac_os    = "arduino-%s-macosx.zip"
linux_32  = "arduino-%s-linux32.tar.xz"
linux_64  = "arduino-%s-linux64.tar.xz"
linux_arm = "arduino-%s-linuxarm.tar.xz"

def getNotes():
    if not os.path.exists(root): os.makedirs(root)

    url = "https://www.arduino.cc/en/Main/ReleaseNotes"
    r = requests.get(url)
    s = r.text

    f = open("%s/notes.html" % root, "w")
    f.write(s)
    f.close()

def getRelease(os_type):
    f = open("%s/notes.html" % root, "r")
    ls = f.readlines()
    f.close()
    os = os_type
    t = os.split("-")[2]
    t = t.replace(".", "-")
    s  = ""
    s2 = ""
    for l in ls:
        if "ARDUINO" in l[:7]:
            l   = l.strip()
            l   = l.replace(' - ', ' ')
            r   = l.split(" ")
            tit = r[0]
            if len(r) > 1:
                ver = r[1]
            else:
                ver = ""
            dat = "---"
            bate = ""
            if ver != "" and ver[:1] == "0":
                continue
            if len(r) > 3:
                #bate = r[2]
                dat  = r[3]
            elif len(r) > 2:
                dat = r[2]
            else:
                dat = ""
                #continue
            #print(l)
            u = home + os % ver
            s_t = "%s-%s %" + str(10-len(ver)) + "s %s%"+str(5-len(bate))+"s %s %"+str(10-len(dat))+"s %s"
            s_l = s_t % (tit, ver, " ", bate, " ", dat, " ", u)
            print(s_l)
            s += s_l + "\n"
            s2 += "<h2><p><a href='%s%s' target='_blank'>%s</p></h2>\n" % (home, os % ver, l)

    s2 = "<html><head><title>arduino IDE download</title></head><body>" + s2 + "</body></html>"

    f = open("%s/arduino_downlist_%s.txt" % (root, t), "w")
    f.write(s)
    f.close()

    f = open("%s/arduino_downlist_%s.html" % (root, t), "w")
    f.write(s2)
    f.close()

if __name__ == "__main__":
    conf = configparser.ConfigParser()
    if not os.path.exists(fn_config):
        root = os.path.expanduser('~') + "/Documents/arduino"
        conf.add_section("main")
        conf.set("main", "root", root)
        conf.write(open(fn_config, "w"))
    else:
        conf.read(fn_config)
        root = conf.get("main", "root")    

    i_len = len(sys.argv)
    if i_len == 2:
        getNotes()
        cmd = sys.argv[1]
        if cmd == "-winzip":
            getRelease(winzip)
        elif cmd == "-winexe":
            getRelease(winexe)
        elif cmd == "-mac_os":
            getRelease(mac_os)
        elif cmd == "-linux_32":
            getRelease(linux_32)
        elif cmd == "-linux_64":
            getRelease(linux_64)
        elif cmd == "-linux_arm":
            getRelease(linux_arm)
        elif cmd == "-h" or cmd == "--help":
            print(readme % s_version)
        elif cmd == "-v" or cmd == "--version":
            print(s_version)
    else:
        print(readme % s_version)
    #print("finish!")
