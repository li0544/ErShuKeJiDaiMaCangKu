﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;


namespace wpcurl
{
    class Program
    {

        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                if (args.Length > 0)
                {
                    string url = args[0];
                    Client c = new Client();
                    c.load(url);
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }            
        }

    }

    public class Client
    {
        WebBrowser webBrowser1;

        public void load(string url)
        {
            webBrowser1 = new WebBrowser();

            webBrowser1.ScriptErrorsSuppressed = true;

            webBrowser1.Navigate(url);

            webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);

            //避免假死，若去掉则可能无法触发 DocumentCompleted 事件。
            while (webBrowser1.ReadyState != System.Windows.Forms.WebBrowserReadyState.Complete)
            {
                System.Windows.Forms.Application.DoEvents(); 
            }
        }

        void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            Console.Write(webBrowser1.Document.Body.InnerHtml);
        }
    }
}
