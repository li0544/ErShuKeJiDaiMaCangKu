def crc16(x):
    b = 0xA001
    a = 0xFFFF
    for byte in x:
        a ^= ord(byte)
        for i in range(8):
            last = a % 2
            a >>= 1
            if last == 1:
                a ^= b
    return a

print(hex(crc16("\x02\x05\x00\x00\x00\x00")))

