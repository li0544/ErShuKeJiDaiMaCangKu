# 二叔科技代码仓库

### 首页

### 目录
| 目录 | 说明 | 作者 |
| --- | --- | --- |
| 0001.arduino_update_py | 获取arduino所有版本下载地址 | water |
| 0002.esp8266_shaolu | esp8266烧录的相关资料 | bugcat |
| 0003.xiaomiiot | 小米网关API示例 | bugcat |
| 0004.weixin_api_index | 获取微信API目录 | water |
| 0005.gamesky | 下载壁纸 | water |
| 0006.TIOBE | 读取编程语言排行榜 | water |
| 0007.wa-ban-info | 蛙板资料 | bugcat |
| 0008.pip_update | 自动升级所有过期的python库 | water |
| 0009.wpcurl | 可以解析javascript脚本的curl命令| water |
| 0010.MH_Z1X_Water | MH_Z1X系列CO2传感器的Arduino库 | water |
| 0011.DownPiInfo | 从官网下载rpi，opi，bpi的详细信息 | water |
| 0012.python_crc16 | 硬件常用的 crc16 算法 | bugcat |
| 0013.httpTool | http工具集 | water |
| 0014.python_mqtt | python mqtt 小示例 | bugcat |
| 0015.python_bottle_demo | bottle简易用法 | bugcat |
