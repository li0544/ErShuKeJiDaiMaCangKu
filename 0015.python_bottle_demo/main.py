from bottle import run, request, route, static_file

@route("/upload", method="POST")
def upload():
	file = request.files.get("file")
	file.save(".", overwrite=True)

@route("/<path:path>")
def staticfile(path):
	return static_file(path, ".")

@route("/")
def index():
	return """<html>
<head>
	<title>upload</title>
</head>
<body>
	<form action="upload" method="POST" enctype="multipart/form-data">
		<input type="file" name="file" />
		<input type="submit" value="upload" />
	</form>
</body>
</html>"""

import logging
logging.root.setLevel(logging.NOTSET)

run(host="0.0.0.0", port="8000") # 开发版，方便调适
# run(host="0.0.0.0", port="8000", server="tornado") # 生产版，效率更高效 （需要 pip install tornado 安装库）