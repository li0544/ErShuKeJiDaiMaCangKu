
#ifndef mh_z1x
#define mh_z1x

#if defined(ARDUINO) && (ARDUINO >= 100)
#include <Arduino.h>
#else
#include <WProgram.h>
#endif

//#define DEBUG
// 错误计数器
#define ERROR_MAX 100

// 使用软串口读取数据
#define MH_Z1X_USE_SOFTWARE_SERIAL

#ifdef MH_Z1X_USE_SOFTWARE_SERIAL
#include "SoftwareSerial.h"
#endif


class MH_Z1X{
public:
    MH_Z1X();
    // 使用 PWM 读取数据
    void read_PWM(int pin);
    void read();
    // 使用串口读取
#ifdef MH_Z1X_USE_SOFTWARE_SERIAL
    SoftwareSerial *m_puart; 
    MH_Z1X(SoftwareSerial &uart);
#else    
    HardwareSerial *m_puart; 
    MH_Z1X(HardwareSerial &uart);
    
#endif
    
    int i_error = 0;
    long co2;
    
};
#endif