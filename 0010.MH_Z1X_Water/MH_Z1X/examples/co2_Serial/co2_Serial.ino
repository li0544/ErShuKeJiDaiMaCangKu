#include "MH_Z1X.h"


#ifdef MH_Z1X_USE_SOFTWARE_SERIAL
#include "SoftwareSerial.h"
SoftwareSerial mySerial(4, 5); // MEAG TX:10, RX:11
MH_Z1X co2(mySerial);
#else
MH_Z1X co2(Serial1);
#endif
void setup() {
  Serial.begin(9600);      
}

void loop() {
  co2.read();
  Serial.print("CO2=");
  Serial.print(co2.co2);
  Serial.println();
  delay(1000);
}
