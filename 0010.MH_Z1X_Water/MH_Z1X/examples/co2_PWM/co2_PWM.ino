#include "MH_Z1X.h"

MH_Z1X co2;
#define pin 11

void setup() {
  Serial.begin(9600);      
}

void loop() {
  co2.read_PWM(pin);     // pin
  Serial.print("CO2=");
  Serial.print(co2.co2);
  Serial.println();
  delay(1000);
}
