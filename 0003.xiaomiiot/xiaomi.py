# -*- encoding: utf-8 -*-
import socket
import time
import struct

sk = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
sk.bind(("0.0.0.0", 9898))

mreq = struct.pack("!4sl", socket.inet_aton("224.0.0.50"), socket.INADDR_ANY)
sk.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

# sk.sendto('{"cmd": "whois"}', ("224.0.0.50", 4321))
# print sk.recvfrom(1024)[0]
# sk.sendto('{"cmd":"get_id_list"}', ("192.168.199.244", 9898))
# print sk.recvfrom(1024)
# sk.sendto('{"cmd":"read", "sid":"158d0001050c75"}', ("192.168.199.244", 9898))
# print sk.recvfrom(1024)[0]

while True:
    print(sk.recvfrom(1024)[0])
