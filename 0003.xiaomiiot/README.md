## 小米网关开发文档地址
https://github.com/louisZL/lumi-gateway-local-api

## 主要开放的API功能为：

设备发现与查询
对设备进行读写操作
设备心跳
设备上报和控制报文格式
传感器上报属性和心跳

➜  0003.xiaomiiot git:(master) python xiaomi.py 
- {"cmd":"report","model":"motion","sid":"","short_id":21871,"data":"{\"status\":\"motion\"}"}
- {"cmd":"heartbeat","model":"gateway","sid":"","short_id":"0","token":"","data":"{\"ip\":\"192.168.199.244\"}"}
- {"cmd":"report","model":"motion","sid":"","short_id":21871,"data":"{\"status\":\"motion\"}"}
- {"cmd":"report","model":"sensor_ht","sid":"","short_id":24111,"data":"{\"temperature\":\"2809\"}"}
- {"cmd":"report","model":"sensor_ht","sid":"","short_id":24111,"data":"{\"humidity\":\"6819\"}"}
