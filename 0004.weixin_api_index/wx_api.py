import codecs
import requests
import json

url_v1 = "http://mp.weixin.qq.com/wiki/home/index.html"
url_v2 = "https://developers.weixin.qq.com/doc/offiaccount/Getting_Started/Overview.html"


def find_str_between(l, s_s, s_e):
    i_s = l.find(s_s) + len(s_s)
    s = l[i_s:]
    if s_e != "":
        i_e = s.find(s_e)
        s = s[:i_e]
    return s


def get_index_v1():
    con = requests.session()
    s = con.get(url_v1).text
    s = s.replace("{", "\n{")
    ls = s.split("\n")
    i1 = 1
    i2 = 1
    s_all = ""
    for l in ls:
        if '"id"' in l:
            id = find_str_between(l, '{"id":"', '"')
            father = find_str_between(l, '"father":"', '"')
            name = find_str_between(l, '"name":"', '"')
            u2 = "https://mp.weixin.qq.com/wiki?t=resource/res_main&id=%s" % id
            s_s = "                     "[:-(len(name) * 2)]
            if father == "0":
                s = "%2d  %s         %s  %s" % (i1, name, s_s, u2)
                print(s)
                i1 += 1
                i2 = 1
            else:
                s = "\t%2d.%02d  %s  %s  %s" % (i1, i2, name, s_s, u2)
                print(s)
                i2 += 1
            s_all += s + "\n"

    f = codecs.open("index.txt", "w", "utf-8")
    f.write(s_all)
    f.close()


def get_index_v2():
    home = 'https://developers.weixin.qq.com/doc/offiaccount/'
    print(url_v2)
    con = requests.session()
    s = con.get(url_v2).content.decode('utf-8')
    url_js = find_str_between(s, 'as="style"><link rel="preload" href="', '"')
    # print(url_js)
    s = con.get(url_js).content.decode('utf-8')
    s = find_str_between(s, '"/doc/offiaccount/":', ',"/doc/oplatform/Downloads/"')

    s = s.replace("{", "\n{")
    # print(s)
    ls = s.split('\n')
    i1 = 1
    i2 = 1
    s_all = ""
    for l in ls:

        title = find_str_between(l, 'title:"', '"')
        path = find_str_between(l, 'path:"', '"')
        s_s = "                         "[:-(len(title) * 2)]
        if title == '':
            continue
        s = '%d %s %s %s%s' % (i1, title, s_s, home, path)
        print(s)
        s_all += '%s\n' % s
        children = find_str_between(l, 'children:[', ']}')
        children = children.replace('[', '\n[')
        ls2 = children.split('\n')
        for l2 in ls2:
            if l2 == '':
                continue
            # print('\t%s' % l2)
            title = find_str_between(l2, '","', '"')
            path = find_str_between(l2, '["', '"')
            s_s = "                     "[:-(len(title) * 2)]
            s = '\t%d.%d %s %s %s%s' % (i1, i2, title, s_s, home, path)
            print(s)
            s_all += '%s\n' % s
            i2 += 1
        i1 += 1
        i2 = 1

    f = codecs.open("index_v2.txt", "w", "utf-8")
    f.write(s_all)
    f.close()


if __name__ == "__main__":
    get_index_v2()
