package gateway

import (
    "net"
)

func ReadFromUDP() string {
    // udp组播
    addr, _ := net.ResolveUDPAddr("udp", "224.0.0.50:9898")
    conn, _ := net.ListenMulticastUDP("udp", nil, addr)

    data := make([]byte, 512)
    conn.ReadFromUDP(data)
    return string(data)
}